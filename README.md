
## Find The Itinerary

---

Each service should be started with command 

**mvn clean spring-boot:run**

in next order:

---

- **discovery-service**

After service started, you can open in browser: 

http://localhost:8762/  

There you can see Eureka UI.

---

- **proxy-gateway-service** 

After service started, all requests to other services can be sent via http://localhost:8080/

---

- **schedules-service**

This service is backed by MongoDb with default configuration.

After service started and registered in discovery service, 
you can open in browser:

http://localhost:8080/schedules-service/
  
There you can see HAL browser that works with JSON responses in HATEOAS format.

---

- **itinerary-search-service**

After service started and registered in discovery service,
you can open in browser:

http://localhost:8080/itinerary-search-service/swagger-ui.html
 
There you can see Swagger UI.

---
