package bulatnikov.yevhen.itinerarysearchservice.controller;

import bulatnikov.yevhen.itinerarysearchservice.service.ItinerarySearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;
import java.util.LinkedHashMap;

@RestController
@RequestMapping("/itinerary/search")
public class ItinerarySearchController {
    @Autowired
    private ItinerarySearchService service;

    @GetMapping
    public ResponseEntity<LinkedHashMap<String, Long>> searchItinerary(@RequestParam @NotBlank String city,
                                                                       @RequestParam @NotBlank String destinationCity) {
        return ResponseEntity.ok(service.search(city, destinationCity));
    }
}
