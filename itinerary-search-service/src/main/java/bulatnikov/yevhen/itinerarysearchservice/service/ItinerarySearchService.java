package bulatnikov.yevhen.itinerarysearchservice.service;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service
public class ItinerarySearchService {
    @Autowired
    private EurekaClient discoveryClient;

    public LinkedHashMap<String, Long> search(String city, String destinationCity) {
        InstanceInfo instances = discoveryClient
                .getNextServerFromEureka("schedules-service", false);
        //todo: here should be some kind of Dijkstra's algorithm implementation
        //todo: using requests to schedules-service
        return null;
    }
}
