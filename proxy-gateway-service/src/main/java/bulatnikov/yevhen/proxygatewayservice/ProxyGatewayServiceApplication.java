package bulatnikov.yevhen.proxygatewayservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class ProxyGatewayServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProxyGatewayServiceApplication.class, args);
	}

}
