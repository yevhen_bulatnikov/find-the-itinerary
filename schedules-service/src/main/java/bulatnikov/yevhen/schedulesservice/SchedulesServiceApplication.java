package bulatnikov.yevhen.schedulesservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchedulesServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchedulesServiceApplication.class, args);
	}

}
