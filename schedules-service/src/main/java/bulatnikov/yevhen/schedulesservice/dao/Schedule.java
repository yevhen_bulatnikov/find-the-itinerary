package bulatnikov.yevhen.schedulesservice.dao;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Document
public class Schedule {
    @Id
    private String id;
    @NotBlank
    private String city;
    @NotBlank
    private String destinationCity;
    @Min(0) @Max(value = 24 * 60 - 1) @DateTimeFormat(pattern = "hh:mm")
    private long departureTime;
    @Min(0) @Max(value = 24 * 60 - 1) @DateTimeFormat(pattern = "hh:mm")
    private long arrivalTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public long getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(long departureTime) {
        this.departureTime = departureTime;
    }

    public long getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(long arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
}
