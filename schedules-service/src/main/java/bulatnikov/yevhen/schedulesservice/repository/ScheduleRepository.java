package bulatnikov.yevhen.schedulesservice.repository;

import bulatnikov.yevhen.schedulesservice.dao.Schedule;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScheduleRepository extends MongoRepository<Schedule, String> {
}
